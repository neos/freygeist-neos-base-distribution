name: neos
recipe: lamp
config:
  php: 8.2
  composer_version: 2.5.8
  webroot: ./Web
  xdebug: false

services:
  appserver:
    ssl: true
    extras:
      - curl -sL https://deb.nodesource.com/setup_20.x | bash -
      - apt update
      - apt -y install software-properties-common gnupg2 apt-transport-https build-essential mc nodejs lsb-release ca-certificates wget
      - npm install -g npm
      - npm install -g nodemon yarn
    config:
      php: .lando/php.ini
    overrides:
      environment:
        XDEBUG_MODE:
        PHP_IDE_CONFIG: "serverName=appserver"
        # FLOW_CONTEXT Production
        FLOW_CONTEXT: Development/Lando
        FLOW_PATH_TEMPORARY_BASE: /tmp/Flow
        FLOW_REWRITEURLS: 1
  database:
    type: mariadb:10.6
    portforward: 3306
    creds:
      user: dev
      password: dev
      database: neos
    config:
      database: .lando/mysql.conf
  phpmyadmin:
    type: phpmyadmin
    ssl: true
    hosts:
      - database
    environment:
      PMA_HOST: database
      PMA_USER: dev
      PMA_PASSWORD: dev
  mailhog:
    type: mailhog
    ssl: true

proxy:
  appserver:
    - neos.local
  phpmyadmin:
    - pma.local
  mailhog:
    - mailhog.local

events:
  post-start:
    - appserver: composer install
    - appserver: ./flow neos.flow:package:rescan
    - appserver: ./flow database:setcharset
    - appserver: ./flow doctrine:migrate
    - appserver: npm install --no-audit --silent
  post-destroy:
    - appserver: rm -rf composer.lock
    - appserver: rm -rf flow
    - appserver: rm -rf flow.bat
    - appserver: rm -rf bin
    - appserver: rm -rf Build
    - appserver: rm -rf Data
    - appserver: rm -rf Packages
    - appserver: rm -rf Web

tooling:
  xdebug-on:
    service: appserver
    description: Enable xdebug for apache.
    cmd:
      - docker-php-ext-enable xdebug
      - /etc/init.d/apache2 reload
      - echo "Xdebug enabled"
    user: root

  xdebug-off:
    service: appserver
    description: Disable xdebug for apache.
    cmd:
      - rm -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
      - /etc/init.d/apache2 reload
      - echo "Xdebug disabled"
    user: root

  profiling-on:
    service: appserver
    description: Enable xdebug profiling for apache.
    cmd:
      - docker-php-ext-enable xdebug
      - echo 'xdebug.mode = debug,develop,profile' >> z
      - echo 'xdebug.profiler_enable = 1' >> z
      - echo 'xdebug.output_dir = "/app/.xdebug/"' >> z
      - echo 'xdebug.profiler_output_name = "cachegrind.out.%u.%p.%r.%s"' >> z
      - mv z /usr/local/etc/php/conf.d/zzz-lando-xdebug-profiling.ini
      - /etc/init.d/apache2 reload
      - echo "Xdebug Profiling enabled"
    user: root

  profiling-off:
    service: appserver
    description: Disable xdebug profiling for apache.
    cmd:
      - rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
      - rm /usr/local/etc/php/conf.d/zzz-lando-xdebug-profiling.ini
      - /etc/init.d/apache2 reload
      - echo "Xdebug Profiling disabled"
    user: root

  php:
    service: appserver

  mysql:
    service: :host
    description: Drop into a mysql shell on a database service
    cmd:
      - mysql -u root
    options:
      host:
        description: The database service to use
        default: mysql
        alias:
          - h

  npm:
    service: appserver
    description: run npm commands through lando
    dir: /app
    cmd:
      - npm

  node:
    service: appserver
    description: run node commands through lando
    dir: /app
    cmd:
      - node

  yarn:
    service: appserver
    description: run yarn commands through lando
    dir: /app
    cmd:
      - yarn

  flow:
    service: appserver
    description: run NEOS Flow commands through lando
    dir: /app
    cmd:
      - ./flow

  composer:
    service: appserver
    description: Run composer commands in the Packages folder
    dir: /app
    cmd:
      - "composer --ansi"
