###############################################################################
###############################################################################
##                                                                           ##
##                     Freygeist Neos Base Distribution                      ##
##                                                                           ##
###############################################################################
###############################################################################

#
# @author Wilhelm Behncke <behncke@sitegeist.de>
# @author Bernhard Schmitt <schmitt@sitegeist.de>
# @author Andreas Freund <freund@sitegeist.de>
# @author Masoud Hedayati <hedayati@sitegeist.de>
# @author Martin Ficzel <ficzel@sitegeist.de>
# @author Dogan Günaydin <guenaydin@sitegeist.de>
# @author Michael Weißberg <entwicklung@freygeist.dev>
#

###############################################################################
#                                VARIABLES                                    #
###############################################################################
SHELL=/bin/bash

###############################################################################
#                                  README                                     #
###############################################################################
.DEFAULT:
readme::
	@printf "\n"
	@printf "\t\t\033[0;1mFreygeist Neos Base Distribution\033[0m\n"
	@printf "\n"
	@printf " Available Targets\n"
	@printf " --------------------------------------------------------------------\n"
	@printf "\033[0;1m install \033[0m \t Install the project\n"
	@printf "\033[0;1m cleanup \033[0m \t Cleanup all files and make a fresh install\n"
	@printf "\033[0;1m lint \033[0m \t\t Lint all sources\n"
	@printf "\033[0;1m build \033[0m \t Build Frontend\n"
	@printf "\033[0;1m watch \033[0m \t Build Frontend (Watch Mode)\n"
	@printf "\033[0;1m up \033[0m \t\t Start a development server\n"
	@printf "\033[0;1m down \033[0m \t\t Stop the development server\n"
	@printf "\033[0;1m prune \033[0m \t Stop the development server and remove all traces of it\n"
	@printf "\033[0;1m restart \033[0m \t Restart the development server\n"
	@printf "\033[0;1m logs \033[0m \t\t Show logs from the development server\n"
	@printf "\033[0;1m ssh \033[0m \t\t Start a shell on the development server\n"
	@printf "\033[0;1m ssh-mariadb \033[0m \t Run the mariadb client on the development server\n"
	@printf "\033[0;1m clone \033[0m \t Clone data from a deployed instance\n"
	@printf "\n"

###############################################################################
#                             INSTALL & CLEANUP                               #
###############################################################################
environment::
	@echo lando $$(lando version)
	@echo mkcert $$(mkcert -version)
	@lando composer --version
	@echo Node $$(lando node --version)
	@echo Yarn $$(lando yarn --version)

install-githooks::
	@if [ -z $${CI+x} ]; then touch ./.git/hooks/pre-commit && \
		echo "#!/bin/sh" > ./.git/hooks/pre-commit && \
		echo "make lint" >> ./.git/hooks/pre-commit && \
		chmod +x ./.git/hooks/pre-commit; fi

install-composer::
	@lando composer update
	@lando composer install

install-yarn::
	@lando yarn
	@lando flow flow:package:rescan

install::
	@mkdir -p Data/Logs
	@$(MAKE) -s install-githooks
	@$(MAKE) -s install-composer
	@$(MAKE) -s install-yarn
	@$(MAKE) -s flush

flush::
	@lando composer flush

remove-artifacts:
	@lando ssh find ./DistributionPackages/ -type f -name '*.js.fusion' -delete
	@lando ssh find ./DistributionPackages/ -type f -name '*.css.fusion' -delete

cleanup::
	@lando composer cleanup:php
	@lando yarn cleanup:node
	@$(MAKE) -s remove-artefacts

###############################################################################
#                                LINTING & QA                                 #
###############################################################################
lint-editorconfig::
	@echo "Lint .editorconfig"
	@lando composer lint:editorconfig

lint-php::
	@echo "Lint PHP Sources".
	@lando composer lint

lint-fe::
	@echo "Lint CSS/TS Sources"
	@lando yarn lint

lint::
	@$(MAKE) -s lint-editorconfig
	@$(MAKE) -s lint-php
	@$(MAKE) -s lint-fe

test::
	@echo "Lint CSS/TS Sources"
	@lando composer test
	@lando yarn test

###############################################################################
#                               FRONTEND BUILD                                #
###############################################################################
build::
	@lando yarn build

watch::
	@lando yarn watch

###############################################################################
#                                  LANDO                                      #
###############################################################################
up::
	@lando start

down::
	@lando stop

prune::
	@lando destroy -y

restart::
	@lando restart

logs::
	@lando logs -f

###############################################################################
#                                  SSH                                        #
###############################################################################
ssh::
	@lando ssh

ssh-mariadb::
	@lando mysql

###############################################################################
#                                CLONE                                        #
###############################################################################
clone::
	@lando composer clone

###############################################################################
#                                DEPLOYMENT                                   #
###############################################################################
deploy-develop::
	@lando composer deploy:develop

deploy-staging::
	@lando composer deploy:staging

deploy-master::
	@lando composer deploy:master
